# flutter bibi视频播放器皮肤控制器
- [博客地址](https://blog.csdn.net/weixin_45952652/article/details/118695887)
- 觉得不错可以给个赞
### 根据播放器原始皮肤根本不符合市场上的需求则需要定制一款自己需要的UI
- 核心文件 components/customFijkPanel
- 业务逻辑 video 需根据自身的业务需求修改

### 播放器插件
- [fijkplayer](https://pub.flutter-io.cn/packages/fijkplayer)
- [fijkplayer官方文档](https://fijkplayer.befovy.com/docs/zh/)
- [volume_controller](https://pub.flutter-io.cn/packages/volume_controller)
- [screen_brightness](https://pub.flutter-io.cn/packages/screen_brightness)
